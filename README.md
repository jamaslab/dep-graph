# Dependency Graph generator

[![pipeline status](https://gitlab.com/jamaslab/dep-graph/badges/master/pipeline.svg)](https://gitlab.com/jamaslab/dep-graph/-/commits/master) [![coverage report](https://gitlab.com/jamaslab/dep-graph/badges/master/coverage.svg)](https://gitlab.com/jamaslab/dep-graph/-/commits/master) 

The project prints out a dependency graph of a series of packages, given the list of packages and their dependencies in a json format.

## Requirements
- Python 3.6+

## Installing the generator
The generator can be installed directly by downloading the .whl file from the GitLab package registry at this address or by adding teh GitLab registry to your Pip environment.

```
pip3 install dep-graph --index-url https://gitlab.com/api/v4/projects/45335027/packages/pypi/simple
```

Alternatively, you can clone the repository in your local environment and run the following commands:

```
pip3 install -r requirements.txt
cd dep_graph_package
python3 setup.py bdist_wheel
pip3 install dist/dep_graph-0.0.1-py3-none-any
```

## Running the generator
The generator can be executed with the following command

```
python3 -m dep_graph
```

This will access by default a file located in  `/tmp/deps.json`.

A file at a different location can be provided as a parameter from command line.

```
python 3 -m dep_graph <path to json file>
```

## Running the tests
Unit tests can be executed with the following command from the project folder.
```
pytest
```

The test coverage can be computed with the following commands:

```
coverage run -m pytest
coverage report -m
```

The linting can be executed as follows:


```
pylint dep_graph_package/dep_graph
```

## Input file format
The input file must have the following format:

```json
{
    "pkg1": ["pkg2", "pkg3"],
    "pkg2": ["pkg3"],
    "pkg3": []
    ...
}
```

In this JSON file, a key represents a package name, and the value is a list of dependencies (package names) for that key.

