import sys

from . import PackageManager

if __name__ == "__main__":
    filename = "/tmp/deps.json"

    if len(sys.argv) == 2:
        filename = sys.argv[1]

    manager = PackageManager.from_file(filename)

    for package in manager.packages:
        print(str(package))
