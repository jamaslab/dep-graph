from __future__ import annotations
import json

from . import Package


class PackageManager:
    '''
    A class that represents a package manager.
    It contains a list of packages.
    '''
    def __init__(self) -> None:
        '''
        Constructor for the PackageManager class.
        '''
        self.__packages = {}

    def __getitem__(self, key: str) -> Package:
        '''
        Returns the package with the given name.
        :param key: The name of the package.
        :return: The package with the given name.
        :raises KeyError: If the package with the given name does not exist.
        '''
        return self.__packages[key]

    def __len__(self) -> int:
        '''
        Returns the number of packages in the package manager.
        :return: The number of packages in the package manager.
        '''
        return len(self.__packages)

    @property
    def packages(self) -> list[Package]:
        '''
        Returns a list of all the packages in the package manager.
        :return: A list of all the packages in the package manager.
        '''
        return list(self.__packages.values())

    def add_package(self, package: Package) -> None:
        '''
        Adds a package to the package manager.
        :param package: The package to add.
        :raises KeyError: If the package already exists.
        '''

        if package.name in self.__packages:
            raise KeyError(f"Package {package.name} already exists")

        self.__packages[package.name] = package

    def from_dict(dict_data: dict) -> PackageManager:
        #pylint: disable=no-self-argument
        '''
        Creates a package manager from a dictionary.
        :param dict_data: The dictionary to create the package manager from.
        :return: The package manager created from the dictionary.
        :raises KeyError: If a dependency of a package does not exist.
        '''
        retval = PackageManager()

        for elem in dict_data:
            retval.add_package(Package(elem))

        for elem in dict_data:
            for dep in dict_data[elem]:
                try:
                    retval[elem].add_dependency(retval[dep])
                except KeyError as e:
                    print(f"Package {dep} not found")
                    raise e

        return retval

    def from_file(filename: str) -> PackageManager:
        #pylint: disable=no-self-argument
        '''
        Creates a package manager from a json file.
        :param filename: The name of the json file to create the package manager from.
        :return: The package manager created from the json file.
        :raises FileNotFoundError: If the file does not exist.
        :raises json.decoder.JSONDecodeError: If the file is not a valid json file.
        :raises TypeError: If the json file does not contain a dictionary.
        :raises KeyError: If a dependency of a package does not exist.
        '''
        try:
            with open(filename, "r", encoding='UTF-8') as f:
                try:
                    data = json.load(f)
                except json.decoder.JSONDecodeError as e:
                    print(f"Error while parsing json file {filename}: {e.msg}")
                    raise e
        except FileNotFoundError as e:
            print(f"File {filename} not found")
            raise e

        if not isinstance(data, dict):
            raise TypeError(f"Expected dict, got {type(data)}")

        return PackageManager.from_dict(data)
