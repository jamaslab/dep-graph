from __future__ import annotations


class Package:
    '''
    A class that represents a package. 
    It contains a name and a list of dependencies.
    '''

    def __init__(self, name: str) -> None:
        '''
        Constructor for the Package class.
        :param name: The name of the package.
        '''
        self.__name = name
        self.__dependencies = []

    def __str__(self, level=1) -> str:
        '''
        Returns a string representation of the package.
        :param level: The level of the package in the dependency graph.
        :return: A string representation of the package.
        '''
        return "".join([f"- {self.__name}"]
                       + ["\n" + "\t" * level
                          + f"{dep.__str__(level+1)}"
                          for dep in self.__dependencies])

    def __eq__(self, __value: Package) -> bool:
        '''
        Checks if two packages are equal.
        :param __value: The package to compare to.
        :return: True if the packages are equal, False otherwise.
        '''
        return self.__name == __value.name

    def __hash__(self) -> int:
        '''
        Returns the hash of the package.
        :return: The hash of the package.
        '''
        return hash(self.__name)

    @property
    def name(self) -> str:
        '''
        Returns the name of the package.
        :return: The name of the package.
        '''
        return self.__name

    @property
    def dependencies(self) -> list[Package]:
        '''
        Returns the dependencies of the package.
        :return: The dependencies of the package.
        '''
        return self.__dependencies

    def add_dependency(self, dependency: Package) -> None:
        '''
        Adds a dependency to the package.
        :param dependency: The dependency to add.
        '''
        self.__dependencies.append(dependency)
