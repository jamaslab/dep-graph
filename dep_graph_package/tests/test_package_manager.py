import os
import json
import pytest

from dep_graph import PackageManager, Package

TEST_FOLDER = os.path.abspath(os.path.dirname(__file__))


def test_add_package():
    manager = PackageManager()
    manager.add_package(Package("pkg1"))
    manager.add_package(Package("pkg2"))
    manager.add_package(Package("pkg3"))

    assert len(manager) == 3


def test_access_packages():
    manager = PackageManager()
    manager.add_package(Package("pkg1"))
    manager.add_package(Package("pkg2"))
    manager.add_package(Package("pkg3"))

    assert manager["pkg1"].name == "pkg1"
    assert manager["pkg2"].name == "pkg2"
    assert manager["pkg3"].name == "pkg3"


def test_access_non_existing_package():
    manager = PackageManager()
    manager.add_package(Package("pkg1"))
    manager.add_package(Package("pkg2"))
    manager.add_package(Package("pkg3"))

    with pytest.raises(KeyError):
        manager["pkg4"]


def test_add_package_twice():
    manager = PackageManager()
    manager.add_package(Package("pkg1"))
    manager.add_package(Package("pkg2"))
    manager.add_package(Package("pkg3"))

    with pytest.raises(KeyError):
        manager.add_package(Package("pkg2"))


def test_return_list_of_packages():
    manager = PackageManager()
    manager.add_package(Package("pkg1"))
    manager.add_package(Package("pkg2"))
    manager.add_package(Package("pkg3"))

    assert manager.packages == [
        Package("pkg1"), Package("pkg2"), Package("pkg3")]


def test_from_dict():
    data = {
        "pkg1": [],
        "pkg2": ["pkg1"],
        "pkg3": ["pkg1", "pkg2"]
    }

    manager = PackageManager.from_dict(data)

    assert manager["pkg1"].name == "pkg1"
    assert manager["pkg2"].name == "pkg2"
    assert manager["pkg3"].name == "pkg3"

    assert manager["pkg1"].dependencies == []
    assert manager["pkg2"].dependencies == [manager["pkg1"]]
    assert manager["pkg3"].dependencies == [manager["pkg1"], manager["pkg2"]]


def test_dependency_not_existing():
    data = {
        "pkg1": [],
        "pkg2": ["pkg1"],
        "pkg3": ["pkg1", "pkg2", "pkg4"]
    }

    with pytest.raises(KeyError):
        PackageManager.from_dict(data)


def test_from_filename():
    manager = PackageManager.from_file(
        os.path.join(TEST_FOLDER, "data", "test_data.json"))

    assert manager["pkg1"].name == "pkg1"
    assert manager["pkg2"].name == "pkg2"
    assert manager["pkg3"].name == "pkg3"

    assert manager["pkg1"].dependencies == [manager["pkg2"], manager["pkg3"]]
    assert manager["pkg2"].dependencies == [manager["pkg3"]]
    assert manager["pkg3"].dependencies == []


def test_from_non_existing_file():
    with pytest.raises(FileNotFoundError):
        PackageManager.from_file(os.path.join(
            TEST_FOLDER, "data", "non_existing_file.json"))


def test_from_invalid_json_file():
    with pytest.raises(json.decoder.JSONDecodeError):
        PackageManager.from_file(os.path.join(
            TEST_FOLDER, "data", "invalid_json.json"))


def test_file_is_not_dict():
    with pytest.raises(TypeError):
        PackageManager.from_file(os.path.join(
            TEST_FOLDER, "data", "file_is_not_dict.json"))
