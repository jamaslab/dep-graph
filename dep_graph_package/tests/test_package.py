from dep_graph import Package


def test_package_name():
    package = Package("test")
    assert package.name == "test"


def test_package_dependencies():
    package = Package("test")
    assert package.dependencies == list()


def test_package_add_dependency():
    package = Package("test")
    package.add_dependency(Package("test2"))
    assert package.dependencies == [Package("test2")]


def test_package_str():
    pkg = Package("test")
    pkg2 = Package("test2")
    pkg3 = Package("test3")
    pkg.add_dependency(pkg2)
    pkg2.add_dependency(pkg3)
    pkg.add_dependency(pkg3)
    assert str(pkg) == "- test\n\t- test2\n\t\t- test3\n\t- test3"


def test_package_hash():
    pkg = Package("test")

    assert hash(pkg) == hash(pkg.name)
