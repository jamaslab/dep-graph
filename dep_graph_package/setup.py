from setuptools import setup, find_packages

setup(
    name="dep_graph",
    version="0.0.1",
    author="Giacomo Lunghi",
    author_email="lunghi.giacomo@gmail.com",
    description="Dependency graph package",
    url="https://gitlab.com/jamaslab/dep-graph",
    packages=['dep_graph'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    install_requires=[

    ],
    python_requires='>=3.6',
)
